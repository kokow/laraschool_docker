FROM php:8.0.2-fpm-alpine

RUN docker-php-ext-install pdo pdo_mysql

WORKDIR /code

ADD ./laraschool /code

RUN chmod +x install.sh

RUN chown www-data:www-data -R /code

RUN chmod -R 755 storage

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install --ignore-platform-reqs

RUN php artisan optimize:clear

RUN php artisan key:generate

ENTRYPOINT ["sh", "/code/install.sh"]
